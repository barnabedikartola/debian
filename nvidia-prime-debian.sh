#!/bin/bash

#gerar arquivo de log
exec 2> >(sudo tee -a /var/log/nvidia-prime-debian.log)
exec 1>&2

#intalador de driver nvidia para DuZeru 3, EmmiOS e Debian
#escrito por Barnabe di Kartola
#tester Barnabe e Luca Aleksandr
#licença GPL 3
#referencias
#http://forum.duzeru.org/thread/preparando-o-duzeru-para-jogatina-instalacao-driver-nvidia-e-outros-ajustes/
#https://bumblebee-project.org/install.html
#https://wiki.debian.org/Bumblebee
#https://wiki.debian.org/NvidiaGraphicsDrivers/Optimus
#https://wiki.debian.org/NvidiaGraphicsDrivers
#https://www.youtube.com/watch?v=YSlTMEmUOBs
#https://github.com/JuliaGPU/CUDAapi.jl/issues/27
#https://backports.debian.org/Instructions/
#mais conhecimento meu acumulado ao longo dos anos

#não fazer perguntas durante a instalação
#export DEBIAN_FRONTEND=noninteractive

#verificar zenity
ckzenity=$(dpkg -l | grep zenity)
if [ -z "$ckzenity" ]; then
	sudo apt install zenity
fi

#verificar se tem placa nvidia
checknvidia=$(lspci | grep -i vga | grep -i nvidia)
if [ -z "$checknvidia" ]; then
	checknvidia3d=$(lspci | grep -i 3d | grep -i nvidia)
	if [ -z "$checknvidia3d" ]; then
	zenity --info --title="Nvidia Detect" --text="Não foram encontradas placas de Video Nvidia"
	exit 0
	fi
fi

#descobrir se é DuZeru, Emmi ou Debian
os=$(lsb_release -i | cut -d ":" -f 2 | sed 's/^ \+//' | sed 's/\t//g')
debversion=$(lsb_release -sc)


#adicionar repositorio contrib non-free temporariamente
#se for debian
echo "deb http://ftp.br.debian.org/debian/ `lsb_release -sc` main contrib non-free
deb http://security.debian.org/debian-security `lsb_release -sc`/updates main contrib non-free
deb http://ftp.br.debian.org/debian/ `lsb_release -sc`-updates main contrib non-free" | sudo tee /etc/apt/sources.list.d/non-free.list

#preparar o sistema p/ instalar o driver de video
sudo apt-get update
sudo apt-get install -y dkms build-essential linux-headers-$(uname -r)
sudo dpkg --add-architecture i386
sudo apt update
sudo apt install -y lib32z1 lib32ncurses5

#dezabilitar driver nouveau
echo 'blacklist nouveau
blacklist lbm-nouveau
options nouveau modeset=0
alias nouveau off
alias lbm-nouveau off' | sudo tee /etc/modprobe.d/blacklist-nouveau.conf

#verificar desligamento do nouveau
checknoveau=$(cat /etc/modprobe.d/nouveau-kms.conf | grep "options nouveau modeset=0")
if [ "$checknoveau" != "options nouveau modeset=0" ]; then	
	echo options nouveau modeset=0 | sudo tee -a /etc/modprobe.d/nouveau-kms.conf
fi


#pre-instalação dos drivers de video, p/ steam, i386 e mineração. (i386 é dependencia da steam)
sudo update-initramfs -u
#se for stretch
if [ "$debversion" = stretch ]; then
	sudo apt install -y -t stretch xserver-xorg
	sudo apt install -y -t stretch libegl1-mesa libegl1-mesa-dev libegl1-mesa-drivers libgbm-dev libgbm1 libgl1-mesa-dev libgl1-mesa-dri libgl1-mesa-glx libglapi-mesa libgles1-mesa libgles1-mesa-dev libgles2-mesa libgles2-mesa-dev libosmesa6 libosmesa6-dev libxatracker-dev libxatracker2 mesa-common-dev mesa-opencl-icd mesa-va-drivers mesa-vdpau-drivers mesa-vulkan-drivers
#se for buster
elif [ "$debversion" = buster ]; then
	sudo apt install -y -t buster xserver-xorg
	sudo apt install -y -t buster libegl1-mesa libegl1-mesa-dev libwayland-egl1-mesa libgbm-dev libgbm1 libgl1-mesa-dev libgl1-mesa-dri libgl1-mesa-glx libglapi-mesa libglvnd-dev libgles2-mesa libgles2-mesa-dev libosmesa6 libosmesa6-dev libxatracker-dev libxatracker2 mesa-common-dev mesa-opencl-icd mesa-va-drivers mesa-vdpau-drivers mesa-vulkan-drivers
fi


#verificar se é driver current ou legacy
sudo apt install nvidia-detect

#se for DuZeru
if [ "$os" = DuZeru ]; then
	dzversion=$(cat /etc/debian_version | cut -d "/" -f 1)
	sudo sed -i "s/stretch/$dzversion/" /usr/bin/nvidia-detect
#se for Emmi
elif [ "$os" = Emmi ]; then
	emmiversion=$(cat /etc/debian_version | cut -d "/" -f 1)
	sudo sed -i "s/stretch/$emmiversion/" /usr/bin/nvidia-detect
#se for debian não precisa de alteração
fi

#detectar versão do driver
nvidiadriver=$(nvidia-detect | head -7 | tail -1 | sed 's/ //g') #mostra a grafia p/ instalação
nvidiadriverversion=$(nvidia-detect | head -7 | tail -1 | sed 's/ //g' | cut -d "-" -f 2) #mostra só se é legacy ou current
nvidiaversion=$(nvidia-detect | head -7 | tail -1 | sed 's/ //g' | cut -d "-" -f 3) #mostra só o numero do driver

#verificar se é placa de video hibrida (notebook) ou sola (desktop)
hybrid=$(lspci | grep -i vga | grep -i intel)
busid=$(lspci | grep -i vga | grep -i nvidia | awk '{ print $1 }' | sed -r "s/.(.\:).(.\..)/\1\2/" | sed 's/\./\:/')
busid3d=$(lspci | grep -i 3d | grep -i nvidia | awk '{ print $1 }' | sed -r "s/.(.\:).(.\..)/\1\2/" | sed 's/\./\:/')
dm=$(cat /etc/X11/default-display-manager | cut -d "/" -f 4)

#instalar driver nvidia sem interação
sudo DEBIAN_FRONTEND=noninteractive apt-get install -y $nvidiadriver

#continuar a parte das higridas
hybrid () {
#teste se switch_nvidia.sh existe, se sim faça um beckup
test -f /usr/local/bin/switch_nvidia.sh && sudo cp /usr/local/bin/switch_nvidia.sh /usr/local/bin/switch_nvidia.sh-bkp
#crie o script switch_nvidia.sh
sudo mkdir -p /usr/local/bin/
echo 'xrandr --setprovideroutputsource modesetting NVIDIA-0
xrandr --auto
xrandr --dpi 96' | sudo tee /usr/local/bin/switch_nvidia.sh
#de permissão de execução ao script switch_nvidia.sh
sudo chmod +x /usr/local/bin/switch_nvidia.sh
#testelighdm=$(grep switch_nvidia.sh /usr/share/lightdm/lightdm.conf.d/40-lightdm-webkit-greeter.conf)
#se ja tiver o 	switch_nvidia.sh no lightdm não faça nada
if [ "$os" = DuZeru ]; then
	testelighdm=$(grep switch_nvidia.sh /usr/share/lightdm/lightdm.conf.d/40-lightdm-webkit-greeter.conf)
	if [ -z "$testelighdm" ]; then
	echo display-setup-script=/usr/local/bin/switch_nvidia.sh | sudo tee -a /usr/share/lightdm/lightdm.conf.d/40-lightdm-webkit-greeter.conf
	fi
elif [ "$os" = Emmi ]; then
	testelighdm=$(grep switch_nvidia.sh /usr/share/lightdm/lightdm.conf.d/01_debian.conf)
	if [ -z "$testelighdm" ]; then
	echo display-setup-script=/usr/local/bin/switch_nvidia.sh | sudo tee -a /usr/share/lightdm/lightdm.conf.d/01_debian.conf
	fi
#se for debian + lightdm
elif [ "$os" = Debian -a "$dm" = lightdm ]; then
	testeseatdefaults=$(grep -i seatdefaults /usr/share/lightdm/lightdm.conf.d/01_debian.conf)
	testelighdm=$(grep switch_nvidia.sh /usr/share/lightdm/lightdm.conf.d/01_debian.conf)
	if [ -z "$testelighdm" -a -z "$testeseatdefaults" ]; then
	echo '[SeatDefaults]
display-setup-script=/usr/local/bin/switch_nvidia.sh' | sudo tee -a /usr/share/lightdm/lightdm.conf.d/01_debian.conf
	elif [ -z "$testelighdm" ]; then
	echo display-setup-script=/usr/local/bin/switch_nvidia.sh | sudo tee -a /usr/share/lightdm/lightdm.conf.d/01_debian.conf
	fi
#se for debian + sddm
elif [ "$os" = Debian -a "$dm" = sddm ]; then
	ckxsetup=$(grep -i switch_nvidia.sh /usr/share/sddm/scripts/Xsetup)
	if [ -z "$ckxsetup" ]; then
	sudo sed -i '$a\/usr/local/bin/switch_nvidia.sh' /usr/share/sddm/scripts/Xsetup
	fi
#se for debian + GDM
elif [ "$os" = Debian -a "$dm" = gdm3 ]; then
	echo '[Desktop Entry]
Type=Application
Name=Optimus
Exec=sh -c "xrandr --setprovideroutputsource modesetting NVIDIA-0; xrandr --auto"
NoDisplay=true
X-GNOME-Autostart-Phase=DisplayServer' | sudo tee /usr/share/gdm/greeter/autostart/optimus.desktop | sudo tee /etc/xdg/autostart/optimus.desktop 
#else
	#se for Sola Fazer
	#sudo apt install -y nvidia-settings nvidia-opencl-common
fi
}

##se for hybrida fazer
if [ -n "$hybrid" -a -n "$busid" ]; then
	#statements
	#teste se o arquvo xorg.conf existe, se sim faça um backup
	test -f /etc/X11/xorg.conf && sudo cp /etc/X11/xorg.conf /etc/X11/xorg.conf-bkp
	#crie um novo xorg.conf
	echo 'Section "Module"
    Load "modesetting"
EndSection

Section "Device"
    Identifier "nvidia"
    Driver "nvidia"
    BusID "PCI:'$busid'"
    Option "AllowEmptyInitialConfiguration"
EndSection' | sudo tee /etc/X11/xorg.conf

hybrid

elif [ -n "$hybrid" -a -n "$busid3d" ]; then
		#statements
	#teste se o arquvo xorg.conf existe, se sim faça um backup
	test -f /etc/X11/xorg.conf && sudo cp /etc/X11/xorg.conf /etc/X11/xorg.conf-bkp
	#crie um novo xorg.conf
	echo 'Section "Module"
    Load "modesetting"
EndSection

Section "Device"
    Identifier "nvidia"
    Driver "nvidia"
    BusID "PCI:'$busid3d'"
    Option "AllowEmptyInitialConfiguration"
EndSection' | sudo tee /etc/X11/xorg.conf

hybrid

fi

#perguntar se quer instalar modulos cuda
if [ "$nvidiadriverversion" = driver ]; then
zenity --question --text="Instlar CUDA?" --text="Voçê gostaria de ativar os modulos CUDA?\nIsso vai consumir em media 1GB de Download!" --ok-label="Yes" --cancel-label="No"

	if [ $? = 0 ] ; then
		sudo apt install -y nvidia-cuda-toolkit nvidia-opencl-common
	fi
fi

#remover repositorio contrib non-free.list
sudo rm /etc/apt/sources.list.d/non-free.list

zenity --info --title="Reiniciar o Computador" --text="Instalação concluida, por favor reinicie o computador."


#desinstalador
echo "sudo mv /etc/modprobe.d/blacklist-nouveau.conf /etc/modprobe.d/blacklist-nouveau.conf-bkp
sudo mv /etc/modprobe.d/nouveau-kms.conf /etc/modprobe.d/nouveau-kms.conf-bkp
sudo mv /etc/X11/xorg.conf /etc/X11/xorg.conf-bkp
sudo mv /usr/local/bin/switch_nvidia.sh /usr/local/bin/switch_nvidia.sh-bkp
sudo rm /usr/share/gdm/greeter/autostart/optimus.desktop 
sudo rm /etc/xdg/autostart/optimus.desktop 
sudo sed -i '/switch_nvidia.sh/d' /usr/share/lightdm/lightdm.conf.d/40-lightdm-webkit-greeter.conf
sudo sed -i '/switch_nvidia.sh/d' /usr/share/lightdm/lightdm.conf.d/01_debian.conf
sudo sed -i '/switch_nvidia.sh/d' /usr/share/sddm/scripts/Xsetup
sudo apt autoremove --purge nvidia*" | sudo tee /opt/nvidia-debian-uninstall.sh
