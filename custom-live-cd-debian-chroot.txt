#!/bin/bash

#como modificar uma ISO de live do debian

#instalar dependencias p/ customização
sudo apt-get install -y dumpet xorriso squashfs-tools gddrescue

#criar uma pasta p/ customização e colocar a iso dentro dela
mkdir custom-debian

copiar o arquivo .iso a ser modificado p/ dentro da pasta custom-debian

#entrar na pasta
cd custom-debian

#descomprimir a iso p/ a pasta custom-iso (a pasta sera criada automaticamente)
xorriso -osirrox on -indev nomedoarquivolive.iso -extract / custom-iso

#descompactar o filesystem.squashfs
sudo unsquashfs custom-iso/live/filesystem.squashfs

#preparar o squashfs-root p/ o chroot
sudo cp /etc/resolv.conf squashfs-root/etc/
sudo mount -o bind /run/ squashfs-root/run
sudo cp /etc/hosts squashfs-root/etc/
sudo mount --bind /dev/ squashfs-root/dev

#entrar em chroot
sudo chroot squashfs-root/
mount none -t proc /proc; mount none -t sysfs /sys; mount none -t devpts /dev/pts


	fazer as modificações
	lembrar q vc está como root, tens q mudar as permissões dos arquivos


#sair do chroot
umount /proc || umount -lf /proc
umount /sys
umount /dev/pts
exit

#desmontar as pastas ### MUITO IMPORTANTE ###
sudo umount -f squashfs-root/proc squashfs-root/sys squashfs-root/dev/pts squashfs-root/dev/ squashfs-root/run

#apagar o filesystem.squashfs original
sudo rm custom-iso/live/filesystem.squashfs

#recomprimir o filesystem.squashfs ## isso demora ##
sudo mksquashfs squashfs-root/ custom-iso/live/filesystem.squashfs

#criar o arquivo isohdpfx.bin
	### trocar o "nomedoarquivolive.iso" pelo nome do arquivo .iso original usado p/ customização ###
sudo dd if=nomedoarquivolive.iso bs=512 count=1 of=custom-iso/isolinux/isohdpfx.bin

#entrar na pasta
cd custom-iso/

#criar a iso modificada
sudo xorriso -as mkisofs -isohybrid-mbr isolinux/isohdpfx.bin -c isolinux/boot.cat -b isolinux/isolinux.bin -no-emul-boot -boot-load-size 4 -boot-info-table -o ../custom-debian.iso .